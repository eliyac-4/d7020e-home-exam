//! examples/timing_resources.rs

// #![deny(unsafe_code)]
#![deny(warnings)]
#![no_main]
#![no_std]

use bkpt_trace::{bkpt_end, bkpt_enter, bkpt_exit};
use cortex_m::asm;
use panic_halt as _;
use stm32f4::stm32f411;

#[rtic::app(device = stm32f4::stm32f411)]
mod app {
    use super::*;

    #[shared]
    struct Shared {
        a: u64, // non atomic data
    }

    #[local]
    struct Local {}

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // Initialize (enable) the monotonic timer (CYCCNT)
        cx.core.DCB.enable_trace();
        cx.core.DWT.enable_cycle_counter();
        unsafe { cx.core.DWT.cyccnt.write(0) };
        asm::bkpt();
        rtic::pend(stm32f411::Interrupt::EXTI0);
        rtic::pend(stm32f411::Interrupt::EXTI1);
        (Shared { a: 0 }, Local {}, init::Monotonics())
    }

    #[idle()]
    fn idle(_cx: idle::Context) -> ! {
        bkpt_end();
        loop {
            continue;
        }
    }

    // will be run first due to higher priority
    #[task(binds = EXTI0, shared = [a], priority = 2)]
    fn exti0(mut cx: exti0::Context) {
        bkpt_enter();
        cx.shared.a.lock(|a| {
            bkpt_enter();
            *a += 1;
            bkpt_exit();
        });
        bkpt_exit();
    }

    #[task(binds = EXTI1, shared= [a], priority = 1)]
    fn exti1(mut cx: exti1::Context) {
        bkpt_enter();
        cx.shared.a.lock(|shared| {
            bkpt_enter();
            *shared += 1;
            bkpt_exit();
        });
        bkpt_exit();
    }
}
// Now we are going to have a look at the resource management of RTIC.
//
// First create an objdump file:
// > cargo objdump --example timing_resources --release -- --disassemble > timing_resources.objdump
//
// Lookup the EXTI0 symbol (RTIC binds the exti0 task to the interrupt vector).
//
// You should find something like:
//
// 080001e0 <EXTI0>:
//  80001e0: 80 b5        	push	{r7, lr}
//  80001e2: 6f 46        	mov	r7, sp
//  80001e4: 40 f2 00 01  	movw	r1, #0
//  80001e8: ef f3 11 80  	mrs	r0, basepri
//  80001ec: 02 be        	bkpt	#2
//  80001ee: 02 be        	bkpt	#2
//  80001f0: c2 f2 00 01  	movt	r1, #8192
//  80001f4: d1 e9 00 23  	ldrd	r2, r3, [r1]
//  80001f8: 01 32        	adds	r2, #1
//  80001fa: 43 f1 00 03  	adc	r3, r3, #0
//  80001fe: c1 e9 00 23  	strd	r2, r3, [r1]
//  8000202: 03 be        	bkpt	#3
//  8000204: 03 be        	bkpt	#3
//  8000206: 80 f3 11 88  	msr	basepri, r0
//  800020a: 80 bd        	pop	{r7, pc}
//
// And:
// 0800020c <EXTI1>:
//  800020c: 80 b5        	push	{r7, lr}
//  800020e: 6f 46        	mov	r7, sp
//  8000210: e0 20        	movs	r0, #224
//  8000212: 02 be        	bkpt	#2
//  8000214: 80 f3 11 88  	msr	basepri, r0
//  8000218: 02 be        	bkpt	#2
//  800021a: 40 f2 00 00  	movw	r0, #0
//  800021e: c2 f2 00 00  	movt	r0, #8192
//  8000222: d0 e9 00 12  	ldrd	r1, r2, [r0]
//  8000226: 01 31        	adds	r1, #1
//  8000228: 42 f1 00 02  	adc	r2, r2, #0
//  800022c: c0 e9 00 12  	strd	r1, r2, [r0]
//  8000230: f0 20        	movs	r0, #240
//  8000232: 03 be        	bkpt	#3
//  8000234: 80 f3 11 88  	msr	basepri, r0
//  8000238: 03 be        	bkpt	#3
//  800023a: 00 20        	movs	r0, #0
//  800023c: 80 f3 11 88  	msr	basepri, r0
//  8000240: 80 bd        	pop	{r7, pc}
//
// While the tasks have exactly the same source code, the generated
// code is slightly different.
//
// Explain in your own words what differs and why?
//
// [Your answer here]
//
// Now `trace` this application.
// Notice: Remember to `flash` the binary.
//
// > cargo build --example timing_resources --release
// > embedded-rust-debugger --chip STM32F411RETx --work-directory /home/pln/courses/d7020e/2021/d7020e_home_exam --elf-file /home/pln/courses/d7020e/2021/d7020e_home_exam/f4xx/target/thumbv7em-none-eabi/release/examples/timing_resources
// >> flash
// Flash successful
// >> Core halted at pc: 0x080002a2, reason: Breakpoint
// >> cycle
// pc: 0x080002a2, cycle counter: 2
// Status: Halted(Breakpoint)
// Core halted at address 0x080002a2
//
// You should now be at the first breakpoint (line 31).
//
// From there we can now run the `trace`.
// >> trace
//
// [Your output here]
//
// Based on the trace, fill out the below table.
//
// CS for a resource R is the time we spend in the lock closure.
//
// Task  | WCET | CS for `a` |
// exti0 |      |            |
// exti1 |      |            |
//
// As seen RTIC implements resource protection efficiently
// by leveraging on the NVIC hardware.
//
// You find a comparison to a typical threaded counterpart `freeRTOS` in Table 1.
// http://www.diva-portal.se/smash/get/diva2:1005680/FULLTEXT01.pdf
//
// Notice, the Rust implementation is significantly faster than the C code version
// of Real-Time For the Masses back in 2013.
//
