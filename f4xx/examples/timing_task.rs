//! examples/timing_task.rs

#![deny(warnings)]
#![no_main]
#![no_std]

use bkpt_trace::{bkpt_end, bkpt_enter, bkpt_exit};
use cortex_m::{asm, peripheral::DWT};
use panic_halt as _;
use stm32f4::stm32f411;

#[rtic::app(device = stm32f4::stm32f411, dispatchers = [EXTI1, EXTI2, EXTI3, EXTI4])]
mod app {
    use super::*;

    #[shared]
    struct Shared {
        #[lock_free]
        dwt: DWT,
    }

    #[local]
    struct Local {}

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // Initialize (enable) the monotonic timer (CYCCNT)
        cx.core.DCB.enable_trace();
        cx.core.DWT.enable_cycle_counter();
        (Shared { dwt: cx.core.DWT }, Local {}, init::Monotonics())
    }

    #[idle(shared = [dwt])]
    fn idle(cx: idle::Context) -> ! {
        unsafe { cx.shared.dwt.cyccnt.write(0) };
        asm::bkpt();
        // measure round trip for interrupt
        bkpt_enter();
        rtic::pend(stm32f411::Interrupt::EXTI0);
        bkpt_exit();

        // measure round trip for soft tasks
        bkpt_enter();
        t1::spawn().unwrap();
        bkpt_exit();

        bkpt_enter();
        t2::spawn([1, 2, 3, 4]).unwrap();
        bkpt_exit();

        bkpt_enter();
        t3::spawn([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]).unwrap();
        bkpt_exit();

        bkpt_end();

        loop {
            continue;
        }
    }

    #[task(binds = EXTI0)]
    fn exti0(_cx: exti0::Context) {
        bkpt_enter();
        bkpt_exit();
    }

    #[task()]
    fn t1(_cx: t1::Context) {}

    #[task()]
    fn t2(_cx: t2::Context, msg: [u32; 4]) {
        bkpt_enter();
        black_box(&msg);
        bkpt_exit();
    }

    #[task()]
    fn t3(_cx: t3::Context, msg: [u32; 16]) {
        bkpt_enter();
        black_box(&msg);
        bkpt_exit();
    }
}

// Ensures that all elements are read to prevent LLVM to optimize out
// the message payload.
fn black_box<T>(t: &[T]) {
    unsafe {
        for i in t {
            core::ptr::read_volatile(&i);
        }
    }
}

// Now we are going to have a look at the scheduling of RTIC tasks
//
// First create an objdump file:
// >  cargo objdump --example timing_task --release  -- --disassemble > timing_task.objdump
//
// Lookup the EXTI0 symbol (RTIC binds the exti0 task to the interrupt vector).
//
// You should find something like:
//
// 0800043c <EXTI0>:
//  800043c: 80 b5        	push	{r7, lr}
//  800043e: 6f 46        	mov	r7, sp
//  8000440: 02 be        	bkpt	#2
//  8000442: 03 be        	bkpt	#3
//  8000444: 00 20        	movs	r0, #0
//  8000446: 80 f3 11 88  	msr	basepri, r0
//  800044a: 80 bd        	pop	{r7, pc}
//
// The application triggers the `exti0` task from `idle`, let's see
// how that pans out.
//
// In this example we use `openocd` to connect to the `stlink` programmer.
// In a separate terminal start `openocd` and let it run in the background.
//
// > openocd -f openocd.cfg
//
// Now we can run `gdb` (see the `.cargo/config.toml`).
//
// > cargo run --example timing_task --release
//
// Then continue to the first breakpoint instruction:
// (gdb) c
// timing_task::app::idle (cx=...) at examples/timing_task.rs:36
// 36              asm::bkpt();
//
// So we see that we are now at line 36 of the program.
//
// (gdb) disassemble
//    0x08000250 <+20>:    movw    r4, #0
//    0x08000254 <+24>:    movt    r12, #57344     ; 0xe000
//    0x08000258 <+28>:    movs    r0, #64 ; 0x40
// => 0x0800025a <+30>:    bkpt    0x0000
//    0x0800025c <+32>:    bkpt    0x0002
//    0x0800025e <+34>:    str.w   r0, [r12]
//    0x08000262 <+38>:    bkpt    0x0003
//
// You see that we hit the `asm::bkpt()` and that the next instruction is our
// `bkpt_enter()`.
//
// (gdb) x 0xe0001004
// 0xe0001004:     0x00000003
//
// Here we see, that we have successfully set the cycle counter to zero.
// The `rtic::pend(stm32f411::Interrupt::EXTI0)` "emulates" the
// arrival/triggering of an external interrupt associated with
// the `exti0` task.
//
// (gdb) c
//
// Will get us the the `bkpt_enter()` break point.
//
// Was the cycle counter increased?
// [ Your answer here? ]
//
// (gdb) continue
// Program received signal SIGTRAP, Trace/breakpoint trap.
// timing_task::app::EXTI0 () at examples/timing_task.rs:12
// 12      #[rtic::app(device = stm32f4::stm32f411, dispatchers = [EXTI1, EXTI2, EXTI3, EXTI4])]
//
// Here `gdb` is able to track that we are in the interrupt handler:
// timing_task::app::EXTI0 ()
// However, in this case no detailed information is available on the source code.
// We can dump the assembly.
//
// (gdb) disassemble
// Dump of assembler code for function timing_task::app::EXTI0:
// 0x0800043c <+0>:     push    {r7, lr}
// 0x0800043e <+2>:     mov     r7, sp
// => 0x08000440 <+4>:     bkpt    0x0002
// 0x08000442 <+6>:     bkpt    0x0003
// 0x08000444 <+8>:     movs    r0, #0
// 0x08000446 <+10>:    msr     BASEPRI, r0
// 0x0800044a <+14>:    po
//
// This corresponds to the `bkpt_enter()` call in `exti0`.
//
// We can track the number of cycles for the HW to pend and dispatch the interrupt.
// (gdb) x 0xe0001004
//
// [Your answer here]
//
// (gdb) continue
//
// timing_task::app::exti0 (_cx=...) at examples/timing_task.rs:65
// 65              bkpt_exit();
//
// In this case gdb correctly finds the correct span, and we can list the source.
//
// (gdb) list
//
// [Your answer here]
//
// Now we can continue to return to idle.
//
// (gdb) continue
//
// What is the current value of the cycle counter?
//
// [Your answer here]
//
// What was the round trip time between `bkpt_enter()` and `bkpt_exit`.
//
// [Your answer here]
//
// Assume we run at 100MHz (the maximum speed for the F411).
// How long time did it take in micro seconds.
//
// [Your answer here]
//
// Notice. Under linux (even with a "real-time" kernel), best case
// for scheduling of processes are in the range of milliseconds.
//
// RTIC also provides software tasks, scheduled by a dispatcher for
// each priority in the system. Each software task can be invoked
// with a message (parameters to the tasks).
//
// In this case `t1` takes no parameters, `t2` an array of 4 words (16 bytes),
// and `t3` an array with 16 words (64 bytes).
//
// Using the same technique as above determine the execution time for
// each task. The tasks with parameters calls the black box (BB) function
// to prevent the payload from being optimized out by the Rustc/LLVM.
//
// Fill out the below table:
//
// BB    the OH of the black box in clock cycles
// WCET  the total time for spawn till exit in clock cycles
// WCET* the actual OH for the spawn and task execution computed as (WCET - BB)
//
// Task | BB | WCET | WCET* |
// t1   | 0  |      |       |
// t2   |    |      |       |
// t3   |    |      |       |
//
// [ Commit your answers ]
//
// For details on the ARM NVIC confer to the document:
// https://community.arm.com/developer/ip-products/processors/b/processors-ip-blog/posts/beginner-guide-on-interrupt-latency-and-interrupt-latency-of-the-arm-cortex-m-processors
