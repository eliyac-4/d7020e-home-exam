//! examples/timing_erb.rs

#![deny(warnings)]
#![no_main]
#![no_std]

use bkpt_trace::{bkpt_end, bkpt_enter, bkpt_exit};
use cortex_m::{asm, peripheral::DWT};
use panic_halt as _;
use stm32f4::stm32f411;

#[rtic::app(device = stm32f4::stm32f411, dispatchers = [EXTI1, EXTI2, EXTI3, EXTI4])]
mod app {
    use super::*;

    #[shared]
    struct Shared {
        #[lock_free]
        dwt: DWT,
    }

    #[local]
    struct Local {}

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // Initialize (enable) the monotonic timer (CYCCNT)
        cx.core.DCB.enable_trace();
        cx.core.DWT.enable_cycle_counter();
        (Shared { dwt: cx.core.DWT }, Local {}, init::Monotonics())
    }

    #[idle(shared = [dwt])]
    fn idle(cx: idle::Context) -> ! {
        unsafe { cx.shared.dwt.cyccnt.write(0) };
        asm::bkpt();
        // measure round trip for interrupt
        bkpt_enter();
        rtic::pend(stm32f411::Interrupt::EXTI0);
        bkpt_exit();

        // measure round trip for soft tasks
        bkpt_enter();
        t1::spawn().unwrap();
        bkpt_exit();

        bkpt_enter();
        t2::spawn([1, 2, 3, 4]).unwrap();
        bkpt_exit();

        bkpt_enter();
        t3::spawn([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]).unwrap();
        bkpt_exit();

        bkpt_end();

        loop {
            continue;
        }
    }

    #[task(binds = EXTI0)]
    fn exti0(_cx: exti0::Context) {
        bkpt_enter();
        bkpt_exit();
    }

    #[task()]
    fn t1(_cx: t1::Context) {}

    #[task()]
    fn t2(_cx: t2::Context, msg: [u32; 4]) {
        bkpt_enter();
        black_box(&msg);
        bkpt_exit();
    }

    #[task()]
    fn t3(_cx: t3::Context, msg: [u32; 16]) {
        bkpt_enter();
        black_box(&msg);
        bkpt_exit();
    }
}

// Ensures that all elements are read to prevent LLVM to optimize out
// the message payload.
fn black_box<T>(t: &[T]) {
    unsafe {
        for i in t {
            core::ptr::read_volatile(&i);
        }
    }
}

// We have seen that you can manually trace and obtain
// cycle accurate execution time measurements using
// `openocd` and `gdb`.
//
// However, this becomes tedious and error prone and we
// seek to automate the process. `gdb` offers automation
// through python scripting but the internal threaded
// design makes it cumbersome.
//
// Here we take the approach using `probe.rs` a
// library for host interaction with remote targets.
//
// We first build the application to trace:
//
// > cargo build --example timing_erb --release
//
// Then start the `embedded-rust-debugger`.
//
// > embedded-rust-debugger --chip STM32F411RETx --work-directory /home/pln/courses/d7020e/2021/d7020e_home_exam --elf-file /home/pln/courses/d7020e/2021/d7020e_home_exam/f4xx/target/thumbv7em-none-eabi/release/examples/timing_erb
// >> flash
// >> trace
// Core is running
// >> pc: 0x0800025a, cycle counter: 3
// Halted on: other
// ...
//
// Paste the complete output here:
// [Your output here]
//
// Compare to the measurements that you obtained by hand
// in `timing_tasks`.
//
// Did you find any difference? If so, what is the deviation?
// [Your answer here]
//
// In the last part you will dig deeper into automation
// and improve the `trace` implementation in the
// `embedded-rust-debugger`.
