# Worst case execution time measurements

In this crate you find some boilerplate examples for characterizing the overhead for RTIC based scheduling and resource protection on the ARM Cortex M architecture.

## Inline assembly

In previous lab you used the nightly compiler with an experimental feature allowing inline assembly.

In this lab we take another approach that works on the stable toolchain to allow assembly functions defined externally to be inlined in the final executable.

For this we need two things:

In the `Cargo.toml` file:

``` toml
[dependencies.cortex-m]
version = "0.7.3"
features = ["linker-plugin-lto"]
```

And in the `.cargo/config.toml`

``` toml
  # Cross language LTO optimization
  "-C",
  "linker-plugin-lto",
```

You can check the generated assembly by:

``` shell
> cargo objdump --example timing_task --release  -- -d  > timing_task.objdump
```

Open the corresponding file and search for `bkpt`, and you will find the assembly instructions inlined in `idle`.

## bkpt #nr

The ARM M3 (v7m) and M4 (v7em) architectures provides the `bkpt #nr` instruction, where `#nr` is stored in the 8-bit immediate field. This allows us to differentiate between 256 different breakpoints.

You can download the [ARMv7-M Architecture Reference Manual](https://developer.arm.com/documentation/ddi0403/ed), and lookup the `bkpt` instruction (A7.7.17).

The [bkpt-trace](https://vesuvio-git.neteq.ltu.se/pln/bkpt-trace) crate provides us with "familiar names" for three extra breakpoint instructions, `bkpt_end` (`bkpt #1`), `bkpt_enter` (`bkpt #2`) and `bkpt_exit` (`bkpt #3`) that we can use for automated tracing.

The underlying `bkpt #nr` instructions are implemented in a separate library (details are a bit messy, so we skip these for now).

## embedded-rust-debugger

In a Master's thesis at LTU, Niklas Lundberg implemented a library [rust-debug](https://github.com/Blinningjr/rust-debug.git) for extracting debug information from `DWARF` sections of `elf` files. Niklas also implemented a debugger using `rust-debug` and the `probe` library.

Here we will use a fork of Niklas debugger that implements additional functionality for reading the (DWT) cycle counter (`cycle`) and performing automated timing measurements (`trace`). 

Fork the crate is [embedded-rust-debugger](https://vesuvio-git.neteq.ltu.se/pln/embedded-rust-debugger). The default branch is named `exam` you can create your own branch if you want (or stay on the `exam` branch).

You can either run the debugger directly from the folder, or better install it as a tool.

``` shell
> cargo install --path .
```

---

## Home Exam Assignments for Grade 5

1. `examples/timing_task` In this assignment you will manually characterize `pend` and `spawn` scheduling overhead.
2. `examples/timing_erb` In this assignment you will use the `embedded-rust-debugger` to automate the process. See the above section `embedded-rust-debugger` to install the tool.
3. `examples/timing_resources` In this assignment you will use the `embedded-rust-debugger` to characterize resource protection overhead.
4. In this assignment you will improve the functionality of the `embedded-rust-debugger`. See below for further details.

## Under the hood of `embedded-rust-debugger`

In this final part you will improve the `embedded-rust-debugger` referred to as `erb` in the following.

`erb` implements a debug server and a debug client for interactive CLI like usage (in the default mode). `erb` also implements Microsoft Debug Adapter Interface (MSDAP) allowing e.g., `vscode` to interact with the debug server over the MS DAP (but we don't use that part here).

To get a basic understanding of the implementation we start by looking into the following files:

- `commands/commands.rs` defines the set of commands (`Commands`) the CLI supports (in your fork `cycle` and `trace` have been added compared to Niklas original set of commands). A command line parser is used to parse the input from the user and create a corresponding `DebugRequest`.

- `commands/request.rs` defines the set of requests (`DebugRequest`) the debug server responds to (in your fork `CycleCounter` and `Trace` have been added, reflecting the corresponding commands).

- `debugger/mod.rs` this is where it all happens! 

  - `fn handle_request(...)` delegates the request (`DebugRequest`) into a corresponding command implementation. E.g., `DebugRequest::CycleCounter => self.cycle_counter_command()`,

  In the corresponding implementation we find:
  ``` rust
  // A simple example of a custom command
  fn cycle_counter_command(&mut self) -> Result<Command> {
      let mut core = self.session.core(0)?;
      let (pc_val, cycle_counter) = read_cycle_counter(&mut core)?;
      println!("pc: {:#010x}, cycle counter: {}", pc_val, cycle_counter);
      drop(core);
      self.status_command()
  }
  ```

  Let us look closer at the `read_cycle_counter` implementation.

  ``` rust
  // Read the cycle counter from the DWT Cycle Counter Register (0xe0001004)
  fn read_cycle_counter(core: &mut probe_rs::Core) -> Result<(u32, u32), probe_rs::Error> {
      let mut buff: Vec<u32> = vec![0; 1];
      core.read_32(0xe0001004, &mut buff)?;
      let pc = core.registers().program_counter();
      let pc_val = core.read_core_reg(pc)?;
      Ok((pc_val, buff[0]))
  }
  ```

  The function returns both the current value of the program counter `pc_val` and the 32 bit cycle counter value `buff[0]`. The interaction with the target (hardware) is done using `probe` which allows us to read memory (`core.read_32(0xe0001004, &mut buff)?` in this case), and get the values of registers `core.read_core_reg(pc)?`. Most functions use the Rust `Result` type allowing error handling. The `?` is used to unwrap the result and on failure return the error to the caller.

  The implementation of `trace` is more advanced and split into two parts `trace_command` and `trace_event`. 

  ``` rust
  // A more advanced stateful command
  fn trace_command(&mut self) -> Result<Command> {
      // set trace mode state
      self.trace = true;
      // continue execution
      self.continue_command()
  }
  ```

  ``` rust
  fn trace_event(&mut self, _pc_val: u32) -> Result<()> {
      let mut core = self.session.core(0)?;
      let (pc_val, cycle_counter) = read_cycle_counter(&mut core)?;
      println!("pc: {:#010x}, cycle counter: {}", pc_val, cycle_counter);

      let nr = read_bkpt(&mut core, pc_val)?;
      let bkpt_type = match nr {
          1 => "end",
          2 => "enter",
          3 => "exit",
          _ => "other",
      };
      println!("Halted on: {}", bkpt_type);
      if nr == 1 {
          // trace end
          self.trace = false;
          self.running = false;
      } else {
          // intermediate break point
          drop(core);
          self.continue_command()?;
      }
      Ok(())
  }
  ```

  The latter function is called when the target is halted (due to a break-point instruction reached).

  The `read_bkpt` function returns the break-point number. If `1` we have reached a `bkpt_end` so we update the `Debugger` state to `trace = false` and `running = false`. Other breakpoints just continue execution. A technical detail here is that we need to `drop(core)` before running `self.continue_command`, to ensure that the aliasing rules of Rust is upheld. (As `core` is a substructure of `self`, it needs to be dropped before any other method on `self` is executed. However, as `core` is not used in the subsequent code a smarter implementation of the rust "borrow checker" could actually allow this code without the explicit `drop`, but better safe than sorry...)

  Let's have a final look at `read_bkpt`:

  ``` rust
  // Retrieve the breakpoint number
  fn read_bkpt(core: &mut probe_rs::Core, pc_val: u32) -> Result<u8> {
      let mut code = [0u8; 2];
      core.read_8(pc_val, &mut code)?;
      if code[1] == 0b1011_1110 {
          // 0b1011_1110 is the binary encoding of the BKPT #NR instruction
          // code[0] holds the breakpoint number #NR (0..255)
          Ok(code[0])
      } else {
          Err(anyhow!("Breakpoint expected"))
      }
  }
  ```
  Here we read two bytes (the `bkpt #nr` instruction is 16 bits). `code[1]` (8 most significant bits) holds the instruction code and we check that against the `0b1011_1110` pattern (see the `bkpt #nr` section above). `code[0]` holds the actual `nr` (8 least significant bits). If the read instruction is not a break point return with a "Breakpoint expected" error.

## Improving ERB

Your first improvement will be to print the trace to a file. The simplest way is to use a fixed file name, open the file in the `trace_command` , log the trace events in the `trace_event` and finally close the file when `bkpt_end` is reached.

Hint: The `Debugger` struct holds the state of the debug session, you can store the file handle there (will become part of `self` in the `Debugger` implementation).

Hint: You can check the [Rust by example](https://doc.rust-lang.org/rust-by-example/std_misc/file.html) for file handling in Rust.

A fixed file name is not super flexible, check how you could improve on that by adding a command line argument (see the `StructOpt` handling in `main` part and pass that on when creating the `Debugger`).

Now let's do some more advanced improvements to `erb`,  for each trace point reached we also read out the current `basepri`. I added basic support to `probe.rs` for reading `extra` information from the ARM v7m core.

As a first test you can in the `cycle_counter_command` add:
``` rust
let extra = core.registers().extra().unwrap();
println!("extra {}", core.read_core_reg(extra)?);
```
You can test this by re-installing the `embedded-rust-debugger`.

``` shell
> cargo install --path .`
```

Re-run the tool on the `timing-resources` and run the `cycle` command when inside the `EXTI1` (inside of the critacal section).

``` 
0800020c <EXTI1>:
 800020c: 80 b5        	push	{r7, lr}
 800020e: 6f 46        	mov	r7, sp
 8000210: e0 20        	movs	r0, #224
 8000212: 02 be        	bkpt	#2
 8000214: 80 f3 11 88  	msr	basepri, r0 <-- here the basepri is set
 8000218: 02 be        	bkpt	#2 <-- here you should run the `cycle` command
 800021a: 40 f2 00 00  	movw	r0, #0
 800021e: c2 f2 00 00  	movt	r0, #8192
 8000222: d0 e9 00 12  	ldrd	r1, r2, [r0]
 8000226: 01 31        	adds	r1, #1
```

You should see the value 57344 popping up. This corresponds to a hardware encoding of several registers stored in one "extra" register. It does not make sense yet.

Use the below information to extract the `basepri` info:

```
ARM DDI 0403E.d (ID070218)
C1.6.3 Debug Core Register Selector Register, DCRSR
... the "extra" register contains:
Bits[31:24] CONTROL.
Bits[23:16] FAULTMASK.
Bits[15:8]  BASEPRI.
Bits[7:0]   PRIMASK.
```

(`extra` was just a name I invented ARM did not specify a name for it.)

So you need some shifting (`>>`) and masking (`& ...`) to get the `basepri` field out.

Once you have implemented the extraction of the `basepri` field you can re-install the `erb` and re-run the experiment. Now you should have a more sensible value for the `basepri`, 224. Which corresponds to the value stored in `r0` in the generated assembly code.

Now you can add this code to the `trace_event` and see that you get tracing output according to the current `basepri` for each trace point.

Combine that with the logging to file you already implemented.

Notice, from a scheduling point of view, WHICH resource we lock is NOT important it is ONLY the ceiling value that is important when determining the blocking. In RTIC the `basepri` value is determined from the ceiling (so its possible to determine the ceiling from the `basepri`.

However we are not done yet. The NVIC hardware and the `basepri` register uses a "hardware" encoding of the "logic" priorities we use to define priorities in RTIC applications. We use "logic" priorities also to reason on the schedulability of our applications.

In RTIC we convert logical priorities to hardware priorities by the following function:

``` rust
#[inline]
pub fn logical2hw(logical: u8, nvic_prio_bits: u8) -> u8 {
    ((1 << nvic_prio_bits) - logical) << (8 - nvic_prio_bits)
}
```

`nvic_prio bits` is a constant determined by the architecture (4 bits in the case of the STM32F411, rendering 16 different priorities). So to get the actual ceiling you need to implement a function `hw2logical` (computing the inverse of `logical2hw`).

So when you have this implemented you should use it to convert the "hardware" priorities to "logical" priorities. Then you can for each trace point print (and log to file) the actual ceiling when entering a critical section.

With this at hand you have implemented the fundamentals for automated WCET and Critical Section for integration with your analysis tool.

Sewing everything together is however beyond the scope of this home exam.

Reaching this point (tracing with logical ceilings) you have deserved a grade 5, well done!!!
