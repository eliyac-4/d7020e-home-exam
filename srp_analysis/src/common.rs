use std::collections::{HashMap, HashSet};

// common data structures

#[derive(Debug, Clone)]
pub struct Task {
    pub id: String,
    pub prio: u8,
    pub deadline: u32,
    pub inter_arrival: u32,
    pub trace: Trace,
}

//#[derive(Debug, Clone)]
#[derive(Debug, Clone)]
pub struct Trace {
    pub id: String,
    pub start: u32,
    pub end: u32,
    pub inner: Vec<Trace>,
}

#[derive(Debug)]
pub struct TaskVector {
    pub task: Task,
    pub response_time: u32,
    pub worst_case: u32,
    pub blocking_time: u32,
    pub preemption_time: u32
}

// Our task set
pub type Tasks = Vec<Task>;

// A map from Task/Resource identifiers to priority
pub type IdPrio = HashMap<String, u8>;

// A map from Task identifiers to a set of Resource identifiers
pub type TaskResources = HashMap<String, HashSet<(String, u32)>>;

// Derives the above maps from a set of tasks
pub fn pre_analysis(tasks: &Tasks) -> (IdPrio, TaskResources) {
    let mut ip = HashMap::new();
    let mut tr: TaskResources = HashMap::new();
    for t in tasks {
        update_prio(t.prio, &t.trace, &mut ip);
        for i in &t.trace.inner {
            update_tr(t.id.clone(), i, &mut tr);
        }
    }
    (ip, tr)
}

// helper functions
fn update_prio(prio: u8, trace: &Trace, hm: &mut IdPrio) {
    if let Some(old_prio) = hm.get(&trace.id) {
        if prio > *old_prio {
            hm.insert(trace.id.clone(), prio);
        }
    } else {
        hm.insert(trace.id.clone(), prio);
    }
    for cs in &trace.inner {
        update_prio(prio, cs, hm);
    }
}

fn update_tr(s: String, trace: &Trace, trmap: &mut TaskResources) {
    if let Some(seen) = trmap.get_mut(&s) {
        // Added WCET
        seen.insert((trace.id.clone(), (trace.end - trace.start)));
    } else {
        let mut hs = HashSet::new();
        // Added WCET
        hs.insert((trace.id.clone(), (trace.end - trace.start)));
        trmap.insert(s.clone(), hs);
    }
    for trace in &trace.inner {
        update_tr(s.clone(), trace, trmap);
    }
}

fn get_wcet(task: &Task) -> u32 {
    let wcet = task.trace.end - task.trace.start;
    wcet
}

fn get_load(wcet: f32, ia_time: f32) -> f32 {
    let load: f32 = wcet / ia_time;
    load
}

fn get_it(task: &Task, tasks: &Tasks, bp: u32) -> u32 {
    let mut sum: u32 = 0;

    for t in tasks {

        let c = get_wcet(t);
        let a = t.inter_arrival;

        if t.prio > task.prio {
            sum += c * (bp as f32 / a as f32).ceil() as u32;
        }
    }

    sum
}

fn get_exact_bp(task: &Task, tasks: &Tasks, base_case: u32, current_value: u32) -> Result<u32, &'static str> {

    // Terminate recurrence if busy period is larger than deadline
    if current_value > task.deadline {
        return Err("Deadline miss");
    }

    // Get new busy period
    let new_busy_period = base_case + get_it(task, tasks, current_value);

    // If recurrence is solved then return the value, else continue recursion
    if new_busy_period == current_value {
        Ok(new_busy_period)
    } else {
        match get_exact_bp(task, tasks, base_case, new_busy_period) {
            Ok(result) => Ok(result),
            Err(e) => Err(e)
        }
    }
}

pub fn load_factor(tasks: &Tasks) -> f32 {
    let mut ltot: f32 = 0.0;

    for t in tasks {
        let wcet = get_wcet(t) as f32;
        let ia_time = t.inter_arrival as f32;
        let load = get_load(wcet, ia_time);

        ltot += load;
    }
    ltot
}

pub fn blocking_time(task: &Task, tasks: &Tasks, tr: &TaskResources) -> u32 {
    let mut block_time: u32 = 0;

    // Check if the task uses any resources at all    
    let task_resources = tr.get(&task.id);
    if !task_resources.is_none() {
        // Go through all tasks
        for t_lower in tasks {
            // Check if any other task has a lower priority
            if t_lower.prio < task.prio {
                // Check if the lower priority task uses any resources
                let t_lower_resources = tr.get(&t_lower.id);
                if !t_lower_resources.is_none() {
                    // Go through all resources of given task
                    for resource in task_resources.unwrap().iter() {
                        // Go through resources of lower priority task
                        for tl_resource in t_lower_resources.unwrap().iter() {
                            // Check if resources match
                            if resource.0 == tl_resource.0 {
                                // Add blocking time of resource
                                if block_time < tl_resource.1 {
                                    block_time = tl_resource.1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    block_time
}

pub fn preemption_time(task: &Task, tasks: &Tasks, approx: bool) -> u32 {
    let busy_period: u32;

    // Check if approximation is used or not
    if approx {
        // Base case
        let (_, tr) = pre_analysis(tasks);
        let base_case = blocking_time(task, tasks, &tr) + get_wcet(task);

        // Get result from recurrence equation
        busy_period = match get_exact_bp(task, tasks, base_case, base_case) {
            Ok(result) => result,
            Err(_) => task.deadline
        };
    } else {
        busy_period = task.deadline;
    }

    return get_it(task, tasks, busy_period)
    
}

pub fn response_time(task: &Task, tasks: &Tasks, tr: &TaskResources, approx: bool) -> u32 {
    let b = blocking_time(task, tasks, tr);
    let c = get_wcet(task);
    let i = preemption_time(task, tasks, approx);

    b + c + i
}

pub fn task_vector(tasks: &Tasks, tr: &TaskResources, approx: bool) -> Vec<TaskVector> {
    let mut task_vec: Vec<TaskVector> = Vec::new();

    for task in tasks {
        let response_time = response_time(task, tasks, tr, approx);
        let worst_case = get_wcet(task);
        let blocking_time = blocking_time(task, tasks, tr);
        let preemption_time = preemption_time(task, tasks, approx);
        
        let task_element = TaskVector {
            task: task.clone(),
            response_time: response_time,
            worst_case: worst_case,
            blocking_time: blocking_time,
            preemption_time: preemption_time
        };

        task_vec.push(task_element);
    }

    task_vec
}